from django.urls import path
from .views import ListFormVaksin, DetailFormVaksin

urlpatterns = [

    path('', ListFormVaksin.as_view()),
    path('<int:pk>/', DetailFormVaksin.as_view())
]