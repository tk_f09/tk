from django.db.models import fields
from rest_framework import serializers
from main import models

class PostinganSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'Nama',
            'DOB',
            'NIK',
            'VaksinKe',
            'Tanggal',
            'Lokasi'
        )
        model = models.tempPost