"""TK_F09 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import stat
from django.contrib import admin
from django.urls import include, path
import form_postingan.urls as form_postingan
from django.urls.conf import include

import form_vaksin.urls as form_vaksin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('form-postingan/', include(form_postingan)),
    path('form/', include(form_vaksin)),
    path('apis/v1/', include('apis.urls')),
    path('data', include('data.urls'))
]
