# TK_F09

## Daftar Anggota

- Alaida Arifah Zahra Akmal
- Ezra Pasha Ramadhansyah
- Luthfi Alnazhary
- Arinil Haq
- Joya Ruth Amanda Pane
- Dzikri Qalam Hatorangan
- M Fadhil Mustari


## Link Herokuapp

https://tk-f09.herokuapp.com/

## Cerita Aplikasi

Kami memutuskan untuk membuat aplikasi seperti Peduli Lindungi. Dalam aplikasi kami, user dapat mengetahui informasi seputar covid seperti jumlah kasus yang sembuh, meninggal, dan yang terinfeksi berdasarkan domisili. Terdapat juga search bar agar user bisa mencari data terkait berdasarkan domisili. Selain itu, pada aplikasi kami terdapat portal informasi mengenai kumpulan postingan yang berguna selama pandemi COVID-19.  Selain itu, user dapat mendaftarkan dirinya untuk mendapatkan vaksin lewat aplikasi kami. Kami harap aplikasi kami dapat bermanfaat dalam membantu penyebaran informasi seputar COVID-19 dan menekan laju penyebaran COVID-19.


## Daftar Modul
1. Halaman login untuk authentication.
2. Halaman beranda.
3. Halaman hasil search.
4. Halaman form pendaftaran vaksin.
5. Halaman sertifikasi vaksin.
6. Halaman form postingan informasi seputar COVID-19.
7. Halaman postingan informasi seputar COVID-19.

## Persona
1. Persona 1 - Bayu: Bayu adalah seorang pria berumur kisaran 30 tahun yang seringkali mengakses internet untuk mencari informasi-informsi yang mungkin berguna untuk dirinya. Dia ingin mencari informasi data-data mengenai update kasus COVID-19.
2. Persona 2 - Mawar: Mawar adalah seorang Ibu-ibu rumah tangga berusia 43 tahun yang sering menghabiskan waktu luangnya untuk berselancar di internet dan melihat konten-konten menarik. Mawar suka bingung apabila ingin mendapatkan tips-tips mutakhir agar dia dan sekeluarga bisa aman selama pandemi.
3. Persona 3 - Angga : Angga merupakan pekerja kantoran di wilayah kuningan yang berusia 25 tahun. Dia ingin mengikuti vaksinasi dan mencoba mendaftarkan dirinya melalui pendaftaran vaksinasi online melalui website.
