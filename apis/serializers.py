from django.db.models import fields
from rest_framework import serializers
from main import models

class PostinganSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'Title',
            'SubTitle',
            'Post',
            'File'
        )
        model = models.tempPost