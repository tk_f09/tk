from django.urls import path
from .views import ListFormPostingan, DetailFormPostingan

urlpatterns = [

    path('', ListFormPostingan.as_view()),
    path('<int:pk>/', DetailFormPostingan.as_view())
]