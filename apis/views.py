from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

# Create your views here.
from main import models
from .serializers import PostinganSerializer
from rest_framework import generics

class ListFormPostingan(generics.ListCreateAPIView):
    queryset = models.tempPost.objects.all()
    serializer_class = PostinganSerializer

class DetailFormPostingan(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.tempPost.objects.all()
    serializer_class = PostinganSerializer