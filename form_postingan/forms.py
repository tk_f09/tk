from django import forms
from django.forms import fields
from main.models import tempPost

class PostinganForm(forms.ModelForm):
    class Meta:
        model = tempPost
        fields='__all__'
        labels = {
            'Title': 'title',
            'SubTitle': 'subTitle',
            'Post': 'body',
            'File': 'file'
        }
