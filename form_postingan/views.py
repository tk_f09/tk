from django.db import connections
from django.shortcuts import render, redirect
from .forms import PostinganForm
from main.models import tempPost
from .models import Postingan
from django.http import JsonResponse
from django.core import serializers
from django.http import HttpResponse
import json

# Create your views here.
def index(request):
    post = tempPost.objects.all().values()
    response = {'posts': post}
    return render(request, 'index.html', response)

def add_post(request):
    if request.method == "POST":
        form = PostinganForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('hasil/')
    else:
        form = PostinganForm()
    
    html = 'form_postingan.html'
    return render(request, html, {'form':form})


