from django.db import models

# Create your models here.
class tempPost(models.Model):
    Title = models.TextField()
    SubTitle = models.TextField(null=True, blank=True)
    Post = models.TextField()
    File = models.TextField(null=True, blank=True)
