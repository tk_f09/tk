from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('tempSearch/', views.tempSearch, name='tempSearch'),
    path('result', views.result, name='result'),
    path('sertifikat_vaksin/', views.sertifikat_vaksin, name='sertifikat_vaksin'),
    path('information/', views.informasi, name='informasi'),
    path('newspage/', views.newspage, name='newspage'),
    path('json', views.json, name='json'),
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('time_json', views.json, name='time_json'),
    path('dataPostingan', views.jsonPostingan, name='jsonPostingan'),
    path('jsonModels', views.jsonModels, name='jsonModels')
]
