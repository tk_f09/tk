from django.http import request
from django.shortcuts import render
from django.db.models import Q
from .models import tempPost
from form_vaksin.models import Vaksin
import requests
from time import localtime, strftime
from django.http import JsonResponse
import datetime
from django.core import serializers
from django.http.response import HttpResponse
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt

def home(request):
    response = requests.get('https://api.kawalcorona.com/indonesia').json()
    data = tempPost.objects.all()
    return render(request, 'main/beranda.html', {'response':response, 'data':data})

def tempSearch(request):
    return render(request, 'main/tempSearch.html',{})

def result(request):
    if request.method == 'GET':
        search = request.GET.get('search')
        post = tempPost.objects.filter(Q(Title__icontains=search))
        return render(request, 'main/result.html',{'results':post , 'search':search})

def sertifikat_vaksin(request):
    data = Vaksin.objects.all()
    response = {'data' : data}
    return render(request, 'main/sertifikat_vaksin.html', response)

def informasi(request):
    newspage = tempPost.objects.all()
    response = {'newspage': newspage}
    return render(request, 'main/informasi.html', response)

def newspage(request):
    newspage = tempPost.objects.all()
    response = {'newspage': newspage}
    return render(request, 'main/newspage.html', response)
    

def json(request):
    t = localtime()
    ret = strftime("%H:%M:%S", t)
    return JsonResponse(ret, safe=False)

def login(request):
    return render(request, 'main/login.html')

def register(request):
    return render(request, 'main/register.html')

def jsonPostingan(request):
    data = serializers.serialize('json', tempPost.objects.all())
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def jsonModels(request):
    if request.method == 'GET':
 
        search = request.GET.get('search')
        post = tempPost.objects.filter(Q(Title__icontains=search))

        O = {}
        i = 0
            
        for one in post:
            O[i] = model_to_dict(one)
            i += 1
            
          
        return JsonResponse(O, safe=False)
