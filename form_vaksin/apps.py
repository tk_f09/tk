from django.apps import AppConfig


class FormVaksinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'form_vaksin'
