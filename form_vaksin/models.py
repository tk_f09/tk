from django.db import models


class Vaksin(models.Model):
    name = models.CharField(max_length=30)
    # TODO Implement missing attributes in Friend model
    dob = models.DateField()
    nik = models.CharField(max_length=30)
    jumlahvaksin = models.CharField(max_length=30)
    tanggalvaksin = models.DateField()
    lokasivaksin = models.CharField(max_length=30)
